<?php
/**
 * @title: Proyecto integrador Ev01 - Página principal
 * @description:  Bienvenida a la aplicación
 *
 * @version    0.1
 *
 * @author ander_frago@cuatrovientos.org
 */
require_once(dirname(__FILE__) . '/app/controllers/indexController.php');
$zapatilla = indexAction();
include 'templates/header.php';

?>
<!-- Bootstrap core CSS
* TODO REVISE Este es el aspecto negativo de esta estructura ya que el código esta duplicado
================================================== -->
<link rel="stylesheet" href=".\assets\css\bootstrap.css">
<link rel="stylesheet" href=".\assets\css\style.css">

<div class="backGroundIndex jumbotron jumbotron-fluid">
  <div id="bienvenida" class="container">
    <h1 class='display-3'>Bienvenido a Álvaro Zapatillas</h1>
    <?php
    if ($loggedin) echo "<span class='badge badge-default'> Has iniciado sesión: ".$user."</span>";
    else           echo "<span class='badge badge-default'> regístrate o inicia sesión para una mejor experiencia.</span>";
    ?>
  </div>
</div>


<h1 clas="fontDIferente" align="center">MEJORES COLABORACIONES</h1>
<!--Carousel Wrapper-->
<div id="carousel-example-2" class="carousel slide carousel-fade z-depth-1-half w-50 mx-auto" data-ride="carousel">
  <!--Indicators-->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-2" data-slide-to="1"></li>
    <li data-target="#carousel-example-2" data-slide-to="2"></li>
  </ol>
  <!--/.Indicators-->
  <!--Slides-->
  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
      <div class="view">
        <img class="d-block w-100" src="https://cdn.forbes.com.mx/2020/07/Air-Jordan-White-Off.jpg" alt="First slide">
        <div class="mask rgba-black-light"></div>
      </div>
      <div class="carousel-caption">
        <h3 class="h3-responsive">Jordan 4 Off-White</h3>
        <p>150€ Retail</p>
      </div>
    </div>
    <div class="carousel-item">
      <!--Mask color-->
      <div class="view">
        <img class="d-block w-100" src="https://rb.gy/iyehvf" alt="Second slide">
        <div class="mask rgba-black-light"></div>
      </div>
      <div class="carousel-caption">
        <h3 class="h3-responsive">Nike x Ben & Jerrys</h3>
        <p>110€ Retail</p>
      </div>
    </div>
    <div class="carousel-item">
      <!--Mask color-->
      <div class="view">
        <img class="d-block w-100" src="https://rb.gy/lmx48s" alt="Third slide">
        <div class="mask rgba-black-light"></div>
      </div>
      <div class="carousel-caption">
        <h3 class="h3-responsive">Jordan 1 Dior</h3>
        <p>5k€ Retail</p>
      </div>
    </div>
  </div>
  <!--/.Slides-->
  <!--Controls-->
  <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
  <!--/.Controls-->
</div>

<div class="container">

            <hr>

            
            <!-- Content Row -->
            <?php for ($i = 0; $i < sizeof($zapatilla); $i+=3) { ?>
              <!--   <div class="card-group">   -->
              <div class="row"> 
                <?php
                for ($j = $i; $j < ($i + 3); $j++) {
                   if (isset($zapatilla[$j])) {
                       
                        echo $zapatilla[$j]->zapatilla2HTML();
                    }
                }
                ?>
                   </div> 
                    <!-- /.row -->
             <?php } ?>
            
            
            
            
            <!-- Footer -->
            <footer class="footer">
                <div class="container">
                    <span class="text-muted">Jordan,LV,Off-White,Gucci</span>
                </div>
            </footer>

        </div>


<!-- Bootstrap core JavaScript
* TODO REVISE Este es el aspecto negativo de esta estructura ya que el código esta duplicado
================================================== -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>

<script src=".\assets\js\bootstrap.js"></script>

</body>

</html>