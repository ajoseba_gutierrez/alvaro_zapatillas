<?php

/* Clase UserDAO.php
 * Clase que aplica el Patrón de Diseño DAO para manejar toda la información de un objeto Usuario.
 * author Eugenia Pérez
 * mailto eugenia_perez@cuatrovientos.org
 */
require_once(dirname(__FILE__) . '/../../persistence/conf/PersistentManager.php');
require_once(dirname(__FILE__) . '/../../app/models/Zapatilla.php');
class ZapatillaDAO {

    //Se define una constante con el nombre de la tabla
    const ZAPATILLA_TABLE = 'zapatilla';

    //Conexión a BD
    private $conn = null;

    //Constructor de la clase
    public function __construct() {
        $this->conn = PersistentManager::getInstance()->get_connection();
    }
    public function selectAll() {
        $query = "SELECT * FROM " . ZapatillaDAO::ZAPATILLA_TABLE;
        $result = mysqli_query($this->conn, $query);
        $zapatillas = array();
        while ($zapatillaBD = mysqli_fetch_array($result)) {

            $zapatilla = new Zapatilla();
            $zapatilla->setIdZapatilla($zapatillaBD["idzapatilla"]);
            $zapatilla->setName($zapatillaBD["name"]);
            $zapatilla->setDescription($zapatillaBD["description"]);
            $zapatilla->setUriPicture($zapatillaBD["uripicture"]);
            $zapatilla->setPrice($zapatillaBD["price"]);
            
            array_push($zapatillas, $zapatilla);
        }
        return $zapatillas;
    }

    /* Función de login de usuario */

    public function login($username, $password) {
        $query = "SELECT * FROM " . ZapatillaDAO::TRIP_TABLE .
                " WHERE username = '" . $username . "' AND password='" .
                $password . "'";
        $result = mysqli_query($this->conn, $query);

        if ($result && mysqli_num_rows($result) > 0) {
            $userBD = mysqli_fetch_array($result);
            $user = new User();
            $user->set_ID($userBD['iduser']);
            $user->set_username($userBD['username']);
            $user->set_password($userBD['password']);

            return $user;
        } 
        return null;
    }

    /*
     * Inserta un usuario en la base de datos. Como la clave primaria es 
     * autogenerada, no es necesario insertarla.
     * Es importante que a los valores a insertar que pongas dentro del 
     * paréntesis VALUES, los encierres en comillas simples.
     */

    public function insert($zapatilla) {
        $query = "INSERT INTO " . ZapatillaDAO::ZAPATILLA_TABLE .
                " (name, description, uriPicture, price) VALUES(?,?,?,?)";
        $stmt = mysqli_prepare($this->conn, $query);
        $name = $zapatilla->getName();
        $description = $zapatilla->getDescription();
        $uriPicture = $zapatilla->getUriPicture();
        $price = $zapatilla->getPrice();
        
        mysqli_stmt_bind_param($stmt, 'sssi', $name, $description, $uriPicture, $price);
        return $stmt->execute();
    }
    
    public function selectById($id) {
        $query = "SELECT name, description, uripicture, price FROM " . ZapatillaDAO::ZAPATILLA_TABLE . " WHERE idZapatilla=?";
        $stmt = mysqli_prepare($this->conn, $query);
        mysqli_stmt_bind_param($stmt, 'i', $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $name, $description, $uripicture, $price);

        $zapatilla = new Zapatilla();
        while (mysqli_stmt_fetch($stmt)) {
            $zapatilla->setIdZapatilla($id);
            $zapatilla->setName($name);
            $zapatilla->setDescription($description);
            $zapatilla->setUriPicture($uripicture);
            $zapatilla->setPrice($price);
       }

        return $zapatilla;
    }
    public function update($zapatilla) {
        $query = "UPDATE " . ZapatillaDAO::ZAPATILLA_TABLE .
                " SET name=?, description=?, uriPicture=?, price=?"
                . " WHERE idzapatilla=?";
        $stmt = mysqli_prepare($this->conn, $query);
        $name = $zapatilla->getName();
        $description= $zapatilla->getDescription();
        $uriPicture = $zapatilla->getUriPicture();
        $price = $zapatilla->getPrice();
        $idZapatilla = $zapatilla->getIdZapatilla();
        mysqli_stmt_bind_param($stmt, 'sssii', $name, $description, $uriPicture, $price, $idZapatilla);
        return $stmt->execute();
    }
    public function delete($id) {
        $query = "DELETE FROM " . ZapatillaDAO::ZAPATILLA_TABLE . " WHERE idzapatilla=?";
        $stmt = mysqli_prepare($this->conn, $query);
        mysqli_stmt_bind_param($stmt, 'i', $id);
        return $stmt->execute();
    }


}

?>
