<?php

class Zapatilla {

    private $idZapatilla;
    private $name;
    private $description;
    private $uripicture;
    private $price;

    public function __construct() {
        
    }

    public function getIdZapatilla() {
        return $this->idZapatilla;
    }

    public function getName() {
        return $this->name;
    }

    public function getDescription() {
        return $this->description;
    }

    public  function getUriPicture() {
        return $this->uripicture;
    }
    
    public  function getPrice() {
        return $this->price;
    }

    public function setIdZapatilla($idZapatilla) {
        $this->idZapatilla = $idZapatilla;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    function setUriPicture($uriPicture) {
        $this->uripicture = $uriPicture;
    }
    
    function setPrice($price) {
        $this->price = $price;
    }

//Función para pintar cada criatura
    function zapatilla2HTML() {
        $result = '<div class=" col-md-4 ">';
         $result .= '<div class="card ">';
          $result .= ' <img class="card-img-top rounded mx-auto d-block avatar" src='.$this->getUriPicture().' alt="Card image cap">';
            $result .= '<div class="card-block">';
            $result .= '<h4 class="d-flex justify-content-end">'.$this->getPrice().'€</h4>'; 
                $result .= '<h2 class="card-title"><a href="app/views/edit.php?id='.$this->getIdZapatilla().'">'.$this->getName().':  </a></h2>';
                $result .= '<p class=" card-text description">'.$this->getDescription().'</p>';
             $result .= '</div>';
             $result .= ' <div  class=" btn-group card-footer" role="group">';
                $result .= '<a type="button" class="btn btn-primary" href="app/views/detail.php?id='.$this->getIdZapatilla().'">ver más...</a>';
                $result .= '<a type="button" class="btn btn-danger" href="app/controllers/deleteController.php?id='.$this->getIdZapatilla().'">Borrar</a> ';
            $result .= ' </div>';
         $result .= '</div>';
     $result .= '</div>';
        
        
        return $result;
    }
    
    
}
