<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../persistence/DAO/ZapatillaDAO.php');
require_once(dirname(__FILE__) . '/../../app/models/Zapatilla.php');
require_once(dirname(__FILE__) . '/../../app/models/validations/ValidationsRules.php');



if ($_SERVER["REQUEST_METHOD"] == "POST") {
//Llamo a la función en cuanto se redirija el action a esta página
    createAction();
}

function createAction() {
    $name = ValidationsRules::test_input($_POST["name"]);
    $description = ValidationsRules::test_input($_POST["description"]);
    $uriPicture = ValidationsRules::test_input($_POST["uriPicture"]);
    $price = ValidationsRules::test_input($_POST["price"]);
    
    
    // TODOD hacer uso de los valores validados 
    $zapatilla = new Zapatilla();
    $zapatilla->setName($_POST["name"]);
    $zapatilla->setDescription($_POST["description"]);
    $zapatilla->setUriPicture($_POST["uriPicture"]);
    $zapatilla->setPrice($_POST["price"]);

    //Creamos un objeto CreatureDAO para hacer las llamadas a la BD
    $zapatillaDAO = new ZapatillaDAO();
    $zapatillaDAO->insert($zapatilla);
    
    header('Location: ../../index.php');
    
}
?>

