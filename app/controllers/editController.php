<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../persistence/DAO/ZapatillaDAO.php');
require_once(dirname(__FILE__) . '/../../app/models/Zapatilla.php');


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    editAction();
}

function editAction() {
    
    $id = $_POST["id"];
    $name = $_POST["name"];
    $description = $_POST["description"];
    $uriPicture = $_POST["uriPicture"];
    $price = $_POST["price"];

    $zapatilla = new Zapatilla();
    $zapatilla->setIdZapatilla($id);
    $zapatilla->setName($name);
    $zapatilla->setDescription($description);
    $zapatilla->setUriPicture($uriPicture);
    $zapatilla->setPrice($price);

    $zapatillaDAO = new ZapatillaDAO();
    $zapatillaDAO->update($zapatilla);

    header('Location: ../../index.php');
}

?>

